<?php

/**
 * @file
 * Install, update and uninstall functions for the pino installation profile.
 */

use Drupal\user\Entity\User;

/**
 * Implements hook_install().
 *
 * Perform actions to set up the site for this profile.
 *
 * @see system_install()
 */
function pino_install() {
  // Set front page to newest members.
  \Drupal::configFactory()->getEditable('system.site')->set('page.front', '/latest')->save(TRUE);

  // Allow only administrators to create accounts.
  $user_settings = \Drupal::configFactory()->getEditable('user.settings');
  $user_settings->set('register', USER_REGISTER_ADMINISTRATORS_ONLY)->save(TRUE);

  // Assign user 1 the "administrator" role.
  $user = User::load(1);
  $user->roles[] = 'administrator';
  $user->save();

  // For some reason, blocks with status: false are not actually disabled.
  // For Seven admin theme, let's disable couple of unnecessary blocks
  \Drupal::configFactory()->getEditable('block.block.seven_account_menu')->set('status', FALSE)->save(TRUE);
  \Drupal::configFactory()->getEditable('block.block.seven_local_tasks')->set('status', FALSE)->save(TRUE);
  \Drupal::configFactory()->getEditable('block.block.seven_main_menu')->set('status', FALSE)->save(TRUE);
  \Drupal::configFactory()->getEditable('block.block.seven_branding')->set('status', FALSE)->save(TRUE);

  // Clear all caches.
  drupal_flush_all_caches();
}

function pino_update_8001() {

  // pino_documents is a new feature module, check if it's installed and install if necessary

  $moduleHandler = \Drupal::service('module_handler');

  if(!$moduleHandler->moduleExists('pino_documents')) {

    $installer = \Drupal::service('module_installer');

    if($installer->install(['pino_documents'])) {

      return t('Documents-module installed successfully');

    } else {

      return t('Could not install Documents-module. Check the documentation for Documents-module requirements or trying installing the module manually.');

    }

  } else {

    return t('Documents-module already installed');

  }

}

function pino_update_8002() {

  // Disable some Seven admin theme blocks that were mistakenly enabled

  // Same functionality as in pino_install() function, check there for more information
  \Drupal::configFactory()->getEditable('block.block.seven_account_menu')->set('status', FALSE)->save(TRUE);
  \Drupal::configFactory()->getEditable('block.block.seven_local_tasks')->set('status', FALSE)->save(TRUE);
  \Drupal::configFactory()->getEditable('block.block.seven_main_menu')->set('status', FALSE)->save(TRUE);
  \Drupal::configFactory()->getEditable('block.block.seven_branding')->set('status', FALSE)->save(TRUE);

}