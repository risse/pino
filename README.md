# Pino #

![Pino Logo](https://www.drupal.org/files/styles/grid-3-2x/public/project-images/pino-logo.png?itok=tOx24TqW)

A Member Manager Software built as a Drupal 8 Distribution. 

For associations, non-profits, NGOs and more.

For more information, please visit https://pinomembers.com or our Drupal.org page: https://www.drupal.org/project/pino

## Installation ##

Please check our documentation: https://pinomembers.com/documentation/

Installation should be as easy as:

`composer create-project risse/pino-project [your-project-name-here]`


