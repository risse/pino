<?php

namespace Drupal\member\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Member entities.
 *
 * @ingroup member
 */
class MemberDeleteForm extends ContentEntityDeleteForm {


}
